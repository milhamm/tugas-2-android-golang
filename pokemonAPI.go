package main

import (
	"encoding/json"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"strconv"
)

type Config struct {
	Server   string
	Database string
}

func (c *Config) Read() {
	if _, err := toml.DecodeFile("config.toml", &c); err != nil {
		log.Fatal(err)
	}
}

type Pokemon struct {
	ID   bson.ObjectId `bson:"_id" json:"id"`
	Name string        `bson:"name" json:"name"`
	Type string        `bson:"type" json:"type"`
}
type response struct {
	Pokemon []Pokemon
}
type PokemonDAO struct {
	Server   string
	Database string
}

var db *mgo.Database

const (
	COLLECTION = "pokemon"
)

func (m *PokemonDAO) Connect() {
	session, err := mgo.Dial(m.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

func (m *PokemonDAO) FindAll(skip string, limit string) ([]Pokemon, error) {
	var pokemon []Pokemon
	ski, _ := strconv.Atoi(skip)
	lim, _ := strconv.Atoi(limit)

	err := db.C(COLLECTION).Find(bson.M{}).Skip(ski).Limit(lim).All(&pokemon)
	return pokemon, err
}

func (m *PokemonDAO) FindOneByName(name string) (Pokemon, error) {
	var pokemon Pokemon
	err := db.C(COLLECTION).Find(bson.M{"name": name}).One(&pokemon)
	return pokemon, err
}

var config = Config{}
var dao = PokemonDAO{}

func getPokemon(w http.ResponseWriter, r *http.Request) {
	pokemon, err := dao.FindAll(r.FormValue("offset"), r.FormValue("limit"))
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	var res response
	res.Pokemon = pokemon
	respondWithJson(w, http.StatusOK, res)
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func init() {
	config.Read()

	dao.Server = config.Server
	dao.Database = config.Database
	dao.Connect()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/pokemon", getPokemon).Methods("POST")

	fmt.Println("Starting Server on localhost:5000")
	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
